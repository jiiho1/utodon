import QtQuick 2.9
import Lomiri.Components 1.3
import QtGraphicalEffects 1.12
import Lomiri.Connectivity 1.0
import "../js/Helper.js" as Helper

Rectangle {
  property var dataObject: null
  property bool detailed: false

  function parseMediaAttachments() {
    let mediaAttachments = dataObject.media_attachments;
    let card = dataObject.card;

    if(mediaAttachments.length > 0) {
      for(let i=0; i<mediaAttachments.length; i++) {
        if(mediaAttachments.length > 1) {mediaAttachmentGrid.cellWidth = (parent.width / 2) - units.gu(3)}
        mediaAttachmentModel.append({src: mediaAttachments[i].preview_url});
      }
    } else if(card != null) {
      /*let component = Qt.createComponent("./AttachedLink.qml")
      let obj = component.createObject(mediaAttachmentColumn, {src: card.image, title: card.title, description: card.description, url: card.url});
      if (obj == null) {
          console.log("Error creating object");
      }*/
      mediaAttachmentGrid.cellHeight = units.gu(6)
      mediaAttachmentGrid.delegate = linkAttachmentDelegate;
      mediaAttachmentModel.append({source: card.image, t: card.title, desc: card.description, link: card.url});
    }
  }

  MouseArea {
      anchors.fill: parent

      enabled: !detailed

      onClicked: {
        if(Connectivity.online ) {
          pageStack.push(Qt.resolvedUrl("../Pages/PostDetailed.qml"), {postObject: parent, dataObject: dataObject})
        }
      }
  }

  anchors {
    left: parent.left
    right: parent.right
  }

  height: childrenRect.height

  color: detailed ? LomiriColors.inkstone : "transparent"

  Rectangle {
    id: referenceRectangle

    anchors {
      left: parent.left
      right: parent.right
      top: parent.top
      leftMargin: settings.margin
      rightMargin: settings.margin
      topMargin: dataObject.referenced_by_id != null || dataObject.reply_user_name != null ? units.gu(2) : 0
    }

    height: units.gu(3)

    visible: dataObject.referenced_by_id != null || dataObject.reply_user_name != null ? true : false

    color: "transparent"

    Row {
      anchors.fill: parent

      spacing: units.gu(1)

      Icon {
        width: units.gu(2)
        height: units.gu(2)
        name: dataObject.reply_user_name != null ? "mail-reply" : "retweet"
      }

      Label {

        text: dataObject.referenced_by_id != null ? Helper.parseEmojis(dataObject.account.emojis, dataObject.referenced_display_name) + " " + i18n.tr("reblogged") : i18n.tr("In reply to") + " " + Helper.parseEmojis(dataObject.account.emojis, dataObject.reply_user_name)
        textSize: Label.Small
      }
    }
  }

  Image {
    id: profilePicture

    anchors {
      left: parent.left
      top: dataObject.referenced_by_id != null || dataObject.reply_user_name != null ? referenceRectangle.bottom : parent.top
      leftMargin: settings.margin
      topMargin: dataObject.referenced_by_id != null || dataObject.reply_user_name != null ? 0 : units.gu(2)
    }

    width: units.gu(7)
    height: units.gu(7)

    source: dataObject.account.avatar_static

    property bool rounded: true
    property bool adapt: true

    layer.enabled: rounded
    layer.effect: OpacityMask {
        maskSource: Item {
            width: profilePicture.width
            height: profilePicture.height
            Rectangle {
                anchors.centerIn: parent
                width: profilePicture.adapt ? profilePicture.width : Math.min(profilePicture.width, profilePicture.height)
                height: profilePicture.adapt ? profilePicture.height : width
                radius: units.gu(1)
            }
        }
    }

    MouseArea {
      anchors.fill: parent

      enabled: !detailed

      onClicked: {
        if(Connectivity.online) {
          pageStack.push(Qt.resolvedUrl("../Pages/User.qml"), {id: dataObject.account.id})
        }
      }
    }
  }

  Rectangle {
    id: postUserInfo

    anchors {
      left: profilePicture.right
      right: parent.right
      top: dataObject.referenced_by_id != null || dataObject.reply_user_name != null ? referenceRectangle.bottom : parent.top
      rightMargin: settings.margin
      topMargin: dataObject.referenced_by_id != null || dataObject.reply_user_name != null ? 0 : units.gu(2)
    }

    property int infoRectangleHeight: displayName.implicitHeight + accountName.implicitHeight

    implicitHeight: infoRectangleHeight < units.gu(7) ? units.gu(7) : infoRectangleHeight
    radius: units.gu(1)
    color: "transparent"//settings.cardColor

    Label {
      id: displayName

      anchors {
        top: parent.top
        left: parent.left
        right: contextMenuButton.left
        leftMargin: settings.margin
        rightMargin: settings.margin
        bottomMargin: units.gu(1)
        topMargin: units.gu(1)
      }

      text: dataObject.account.display_name != "" ? Helper.parseEmojis(dataObject.account.emojis, dataObject.account.display_name) : dataObject.account.username
      textFormat: Text.RichText
      wrapMode: Text.Wrap
    }

    Label {
      id: postCreated

      anchors {
        top: displayName.bottom
        left: parent.left
        leftMargin: settings.margin
        topMargin: units.gu(1)
      }

      text: Helper.formatCreationDate(dataObject.created_at)
      textSize: Label.Small
    }

    Label {
      id: accountName

      anchors {
        top: displayName.bottom
        left: postCreated.right
        right: contextMenuButton.left
        leftMargin: units.gu(1)
        rightMargin: settings.margin
        topMargin: units.gu(1)
      }

      text: "@" + Helper.limitString(dataObject.account.acct, accountName.width / units.gu(1))
      textSize: Label.Small
    }

    Button {
      id: contextMenuButton

      anchors {
        top: parent.top
        right: parent.right
        bottom: parent.bottom
      }

      width: units.gu(5)

      color: "transparent"

      iconName: "contextual-menu"
    }

    MouseArea {
      anchors.fill: parent

      enabled: !detailed

      onClicked: {
        //if(Connectivity.online) {
          pageStack.push(Qt.resolvedUrl("../Pages/User.qml"), {id: dataObject.account.id})
        //}
      }
    }
  }

  Label {
    id: postSpoilerLabel

    visible: dataObject.spoiler_text != ""

    anchors {
      top: postUserInfo.bottom
      left: parent.left
      right: parent.right
      topMargin: settings.margin
      leftMargin: settings.margin * 2
      rightMargin: settings.margin * 2
    }

    onLinkActivated: Qt.openUrlExternally(link)
    wrapMode: Text.Wrap
    textFormat: Text.RichText
    text: Helper.linkStyleFormat(Helper.parseEmojis(dataObject.emojis, dataObject.spoiler_text));
  }

  Button {
    id: showSpoilerButton

    visible: dataObject.spoiler_text != ""

    anchors {
      top: postSpoilerLabel.bottom
      left: parent.left
      right: parent.right
      leftMargin: settings.margin * 2
      rightMargin: settings.margin * 2
      topMargin: units.gu(1)
    }

    color: "transparent"

    Row {
      anchors {
        left: parent.left
      }

      spacing: units.gu(1)

      Icon {
        id: showSpoilerIcon
        width: units.gu(3)
        height: units.gu(3)
        name: "down"
      }

      Label {
        id: showSpoilerText
        text: i18n.tr("Show more")
        color: settings.subColor
      }

      /*Label {
        visible: dataObject.sensitive
        text: i18n.tr("Sensitive content")
        color: theme.name == "Lomiri.Components.Themes.SuruDark" ? "#ED3146" : "#C7162B"
      }*/
    }

    onClicked: {
      if(!postContent.visible) {
        showSpoilerText.text = i18n.tr("Show less")
        showSpoilerIcon.name = "up"
        showSpoilerButton.visible = false
        postContent.visible = true
        postContent.text = Helper.linkStyleFormat(Helper.parseTags(dataObject.tags, Helper.parseEmojis(dataObject.emojis, dataObject.content)))
      }// else {
        //showSpoilerText.text = i18n.tr("Show more")
        //showSpoilerIcon.name = "down"
        //postContent.visible = false
        //postContent.text = ""
      //}
    }
  }

  Label {
    id: postContent

    visible: dataObject.spoiler_text == ""

    anchors {
      top: showSpoilerButton.visible ? showSpoilerButton.bottom : postSpoilerLabel.text != "" ? postSpoilerLabel.bottom : postUserInfo.bottom
      left: parent.left
      right: parent.right
      topMargin: settings.margin
      leftMargin: settings.margin * 2
      rightMargin: settings.margin * 2
    }

    onLinkActivated: Qt.openUrlExternally(link)
    wrapMode: Text.Wrap
    textFormat: Text.RichText
    text: showSpoilerButton.visible ? "" : Helper.linkStyleFormat(Helper.parseTags(dataObject.tags, Helper.parseEmojis(dataObject.emojis, dataObject.content)));
  }

  Button {
    id: sensitiveButton

    anchors.horizontalCenter: parent.horizontalCenter
    anchors.top: showSpoilerButton.visible && postContent.visible != visible ? showSpoilerButton.bottom : postContent.bottom
    anchors.topMargin: settings.margin
    anchors.bottomMargin: settings.margin

    visible: dataObject.sensitive && (dataObject.media_attachments.length > 0 || dataObject.card != null)

    text: i18n.tr("Display sensitive content")

    onClicked: {
      parseMediaAttachments()
      sensitiveButton.visible = false
    }
  }

  Rectangle {
    id: mediaAttachmentColumn

    anchors {
      top: showSpoilerButton.visible && postContent.visible != visible ? showSpoilerButton.bottom : postContent.bottom
      left: parent.left
      right: parent.right
      topMargin: settings.margin
      leftMargin: settings.margin + units.gu(1.5)
      rightMargin: settings.margin
    }

    height: mediaAttachmentGrid.contentHeight

    color: "transparent"

    ListModel {
        id: mediaAttachmentModel
    }

    Component {
        id: imageAttachmentDelegate
        Item {
            width: mediaAttachmentGrid.cellWidth; height: mediaAttachmentGrid.cellHeight
            AttachedImage {source: src}
        }
    }

    Component {
      id: linkAttachmentDelegate
      Item {
          width: mediaAttachmentGrid.cellWidth; height: mediaAttachmentGrid.cellHeight
          AttachedLink {src: source; title: t; description: desc; url: link}
      }
    }

    GridView {
      id: mediaAttachmentGrid

      visible: !sensitiveButton.visible

      anchors.fill: parent
      cellWidth: parent.width - units.gu(1); cellHeight: units.gu(20)
      model: mediaAttachmentModel
      delegate: imageAttachmentDelegate
      flow: GridView.FlowLeftToRight
      interactive: false
    }
  }

  Rectangle {
    id: postObjectActionRectangle

    anchors {
      top: sensitiveButton.visible? sensitiveButton.bottom : mediaAttachmentColumn.bottom
      left: parent.left
      right: parent.right
      leftMargin: settings.margin
      rightMargin: settings.margin
    }

    height: units.gu(7)

    color: "transparent"

    Button {
      id: replyButton

      anchors {
        top: parent.top
        bottom: parent.bottom
        left: parent.left
      }

      width: parent.width/4

      color: "transparent"

      Row {
        anchors.centerIn: parent

        spacing: units.gu(1)

        Icon {
          width: units.gu(3)
          height: units.gu(3)
          name: "mail-reply"
        }

        Label {
          visible: !detailed

          text: dataObject.replies_count
          textSize: Label.Large
        }
      }
    }

    Button {
      id: reblogButton

      anchors {
        top: parent.top
        bottom: parent.bottom
        left: replyButton.right
      }

      width: parent.width/4

      color: "transparent"

      Row {
        anchors.centerIn: parent

        spacing: units.gu(1)

        Icon {
          width: units.gu(3)
          height: units.gu(3)
          name: "retweet"
        }

        Label {
          visible: !detailed

          text: dataObject.reblogs_count
          textSize: Label.Large
        }
      }

    }

    Button {
      id: favoriteButton

      anchors {
        top: parent.top
        bottom: parent.bottom
        left: reblogButton.right
      }

      width: parent.width/4

      color: "transparent"

      Row {
        anchors.centerIn: parent

        spacing: units.gu(1)

        Icon {
          width: units.gu(3)
          height: units.gu(3)
          name: "non-starred"
        }

        Label {
          visible: !detailed

          text: dataObject.favourites_count
          textSize: Label.Large
        }
      }
    }

    Button {
      id: shareButton

      anchors {
        top: parent.top
        bottom: parent.bottom
        left: favoriteButton.right
      }

      width: parent.width/4

      color: "transparent"

      Row {
        anchors.centerIn: parent

        spacing: units.gu(1)

        Icon {
          width: units.gu(3)
          height: units.gu(3)
          name: "share"
        }
      }
    }
  }

  Rectangle {
    anchors.top: postObjectActionRectangle.bottom

    height: 1
    width: parent.width

    color: settings.subColor
  }

  Component.onCompleted: {
    if(!sensitiveButton.visible)
      parseMediaAttachments()
  }
}
