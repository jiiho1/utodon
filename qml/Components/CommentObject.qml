import QtQuick 2.9
import Lomiri.Components 1.3
import "../js/Helper.js" as Helper

Rectangle {
  property var dataObject: null

  function parseMediaAttachments() {
    let mediaAttachments = dataObject.media_attachments;


    for(let i=0; i<mediaAttachments.length; i++) {
      let component = Qt.createComponent("./AttachedImage.qml")
      let obj = component.createObject(mediaAttachmentColumn, {
        src: mediaAttachments[i].preview_url,
        sensitive: dataObject.sensitive,
        w: mediaAttachments[i].meta.small.width,
        h: mediaAttachments[i].meta.small.height
      });
      if (obj == null) {
          // Error Handling
          console.log("Error creating object");
      }
    }
  }

  MouseArea {
      anchors.fill: parent
      onClicked: {
          pageStack.push(Qt.resolvedUrl("../Pages/PostDetailed.qml"), {postObject: this, dataObject: dataObject})
      }
  }

  anchors {
    left: parent.left
    right: parent.right
  }

  height: childrenRect.height

  color: "transparent"

  Image {
    id: profilePicture

    anchors {
      left: parent.left
      top: parent.top
      leftMargin: settings.margin
    }

    width: units.gu(7)
    height: units.gu(7)

    source: dataObject.account.avatar_static
  }

  Label {
    id: displayName

    anchors {
      top: parent.top
      left: profilePicture.right
      right: parent.right
      leftMargin: settings.margin
      rightMargin: settings.margin
      bottomMargin: units.gu(1)
    }

    text: dataObject.account.display_name
    textSize: Label.Large
    wrapMode: Text.Wrap
  }

  Label {
    id: accountName

    anchors {
      top: displayName.bottom
      left: profilePicture.right
      leftMargin: settings.margin
    }

    text: dataObject.account.acct
    textSize: Label.Small
  }

  Label {
    id: postCreated

    anchors {
      top: displayName.bottom
      left: accountName.right
      leftMargin: settings.margin
    }

    text: "3 D"
    textSize: Label.Small
  }

  /*Button {
    id: contextMenuButton

    anchors {
      top: parent.top
      right: parent.right
      rightMargin: settings.margin
    }

    height: units.gu(5)
    width: units.gu(5)

    color: "transparent"

    iconName: "contextual-menu"
  }*/

  Label {
    id: postContent

    anchors {
      top: profilePicture.bottom
      left: parent.left
      right: parent.right
      topMargin: settings.margin
      leftMargin: settings.margin
      rightMargin: settings.margin
    }

    onLinkActivated: Qt.openUrlExternally(link)
    wrapMode: Text.Wrap
    textFormat: Text.RichText
    text: Helper.linkStyleFormat(dataObject.content);
  }

  Button {
    id: sensitiveButton

    anchors.horizontalCenter: parent.horizontalCenter
    anchors.top: postContent.bottom
    anchors.topMargin: settings.margin
    anchors.bottomMargin: settings.margin

    visible: dataObject.sensitive && dataObject.media_attachments.length > 0

    text: i18n.tr("Display sensetive content")

    onClicked: sensitiveButton.visible = false
  }

  Column {
    id: mediaAttachmentColumn

    visible: !sensitiveButton.visible

    anchors {
      top: postContent.bottom
      left: parent.left
      right: parent.right
      topMargin: settings.margin
    }

    spacing: units.gu(1)
  }

  Rectangle {
    anchors {
      top: sensitiveButton.visible? sensitiveButton.bottom : mediaAttachmentColumn.bottom
      left: parent.left
      right: parent.right
      leftMargin: settings.margin
      rightMargin: settings.margin
      topMargin: settings.margin * 2
    }

    height: childrenRect.height

    Button {
      id: replyButton

      anchors {
        top: parent.top
        bottom: parent.bottom
        left: parent.left
      }

      width: parent.width/4

      color: "transparent"

      Row {
        anchors.centerIn: parent

        spacing: units.gu(1)

        Icon {
          width: units.gu(3)
          height: units.gu(3)
          name: "mail-reply"
        }

        Label {
          text: dataObject.replies_count
          textSize: Label.Large
        }
      }
    }

    Button {
      id: reblogButton

      anchors {
        top: parent.top
        bottom: parent.bottom
        left: replyButton.right
      }

      width: parent.width/4

      color: "transparent"

      Row {
        anchors.centerIn: parent

        spacing: units.gu(1)

        Icon {
          width: units.gu(3)
          height: units.gu(3)
          name: "retweet"
        }

        Label {
          text: dataObject.reblogs_count
          textSize: Label.Large
        }
      }

    }

    Button {
      id: favoriteButton

      anchors {
        top: parent.top
        bottom: parent.bottom
        left: reblogButton.right
      }

      width: parent.width/4

      color: "transparent"

      Row {
        anchors.centerIn: parent

        spacing: units.gu(1)

        Icon {
          width: units.gu(3)
          height: units.gu(3)
          name: "non-starred"
        }

        Label {
          text: dataObject.favourites_count
          textSize: Label.Large
        }
      }
    }

    Button {
      id: shareButton

      anchors {
        top: parent.top
        bottom: parent.bottom
        left: favoriteButton.right
      }

      width: parent.width/4

      color: "transparent"

      Row {
        anchors.centerIn: parent

        spacing: units.gu(1)

        Icon {
          width: units.gu(3)
          height: units.gu(3)
          name: "share"
        }
      }
    }
  }

  Component.onCompleted: parseMediaAttachments()
}
