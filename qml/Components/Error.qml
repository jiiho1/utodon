import QtQuick 2.9
import Lomiri.Components 1.3
import QtGraphicalEffects 1.12

Rectangle {
  property string errorText: ""

  anchors {
    left: parent.left
    right: parent.right
    leftMargin: settings.margin
    rightMargin: settings.margin
  }

  implicitHeight: errorTextLabel.implicitHeight + settings.margin

  color: "#FA5858"
  opacity: 0.8
  radius: units.gu(0.5)

  Label {
    id: errorTextLabel

    anchors.centerIn: parent

    color: "white"

    text: errorText
    wrapMode: Text.WordWrap
  }

  Timer {
    interval: 5000; running: true; repeat: false
    onTriggered: parent.destroy();
  }
}
