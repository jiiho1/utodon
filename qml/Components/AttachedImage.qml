import QtQuick 2.7
import Lomiri.Components 1.3
import QtGraphicalEffects 1.12

Image {
  id: attachedImage

  visible: !sensitiveButton.visible

  anchors.fill: parent
  anchors.horizontalCenter: parent.horizontalCenter
  anchors.leftMargin: units.gu(1)
  anchors.rightMargin: units.gu(1)
  anchors.topMargin: units.gu(2)

  fillMode: "PreserveAspectCrop"
  sourceSize.width: parent.width
  sourceSize.height: 0

  property bool rounded: true
  property bool adapt: true

  layer.enabled: rounded
  layer.effect: OpacityMask {
      maskSource: Item {
          width: attachedImage.width
          height: attachedImage.height
          Rectangle {
              anchors.centerIn: parent
              width: attachedImage.adapt ? attachedImage.width : Math.min(attachedImage.width, attachedImage.height)
              height: attachedImage.adapt ? attachedImage.height : width
              radius: units.gu(0.5)
          }
      }
  }
}
