import QtQuick 2.7
import Lomiri.Components 1.3
import QtGraphicalEffects 1.12
import "../js/Helper.js" as Helper

Rectangle {
  property string src: ""
  property string title: ""
  property string description: ""
  property string url: ""

  implicitHeight: Math.max(labelLinkDescription.implicitHeight, labelLinkDescription.implicitHeight) < imageLink.height ? imageLink.height : Math.max(labelLinkDescription.implicitHeight, labelLinkDescription.implicitHeight)
  width: parent.width

  color: "transparent"

  MouseArea {
    anchors.fill: parent
    onClicked: {
        Qt.openUrlExternally(url);
    }
  }

  Image {
    id: imageLink

    anchors {
      left: parent.left
      top: parent.top
      leftMargin: units.gu(0.5)
    }

    width: units.gu(6)
    height: units.gu(6)

    fillMode: "PreserveAspectCrop"

    source: url != null ? src : "stock-image"

    property bool rounded: true
    property bool adapt: true

    layer.enabled: rounded
    layer.effect: OpacityMask {
        maskSource: Item {
            width: imageLink.width
            height: imageLink.height
            Rectangle {
                anchors.centerIn: parent
                width: imageLink.adapt ? imageLink.width : Math.min(imageLink.width, imageLink.height)
                height: imageLink.adapt ? imageLink.height : width
                radius: units.gu(0.5)
            }
        }
    }
  }

  Rectangle {
    anchors {
      left: imageLink.right
      top: parent.top
      right: parent.right
      leftMargin: settings.margin/2
      rightMargin: settings.margin/2
    }

    Label {
      id: labelLinkTitle
      anchors {
        left: parent.left
        right: parent.right
        top: parent.top
      }

      text: Helper.limitString(title, 30)
      wrapMode: Text.Wrap
    }

    Label {
      id: labelLinkDescription
      anchors {
        left: parent.left
        right: parent.right
        top: labelLinkTitle.bottom
      }

      text: Helper.limitString(description, 60)
      textSize: Label.Small
      wrapMode: Text.Wrap
      color: settings.subColor
    }
  }

  /*Rectangle {
    anchors {
      bottom: parent.bottom
      left: parent.left
      right: parent.right
    }

    height: units.gu(10)//linkTitle.contentHeight + linkDescription.height

    color: "#FFFFFF"
    opacity: 0.9

    clip: true

    Label {
      id: linkTitle

      anchors {
        top: parent.top
        left: parent.left
        right: parent.right
        topMargin: settings.margin / 2
        leftMargin: settings.margin / 2
        rightMargin: settings.margin / 2
      }

      color: "black"

      text: title
      textSize: Label.Large
    }

    Label {
      id: linkDescription
      anchors {
        top: linkTitle.bottom
        left: parent.left
        right: parent.right
        bottom: parent.bottom
        leftMargin: settings.margin / 2
        rightMargin: settings.margin / 2
        bottomMargin: settings.margin / 2
      }

      color: "black"

      text: "Descriptiondddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd"
      wrapMode: Text.Wrap
    }
  }*/
}
