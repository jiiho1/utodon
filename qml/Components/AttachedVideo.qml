import QtQuick 2.7
import Lomiri.Components 1.3
import QtGraphicalEffects 1.12
import QtMultimedia 5.0

Item {
  id: attachedVideo

  property string src: ""

  visible: !sensitiveButton.visible

  anchors.horizontalCenter: parent.horizontalCenter

  width: root.width > root.height ? root.height - units.gu(4) : parent.width
  height: parent.children.lenght > 1 ? units.gu(20): 0

  MediaPlayer {
      id: mediaplayer
      source: parent.src
  }
  VideoOutput {
      anchors.fill: parent
      source: mediaplayer
  }
  MouseArea {
      id: playArea
      anchors.fill: parent
      onPressed: mediaplayer.play();
  }
}
