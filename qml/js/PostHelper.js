function createPost(component, data) {
  // Only create post with sensitive content, if this action is allowed
  if(!settings.displaySensitiveContent && data.sensitive) {
    return null;
  }

  // Checking for non changeable deny filter words and do no create post,
  // unless that filter was explicitely set to allow
  if (root.denyFilter.some(v => data.content.includes(v)) && !settings.allowFilter.some(v => data.content.includes(v))) {
    return null;
  }

  // Checking for user deny filter words
  if (settings.denyFilter.some(v => data.content.includes(v)) && !settings.allowFilter.some(v => data.content.includes(v))) {
    return null;
  }

  // If post is actually a reblog, make the reblog the actual post.
  if(data.reblog != null) {
    let reblogged_by_id = data.id
    let reblogged_display_name = ""
    if(data.account.display_name != "") {
      reblogged_display_name = data.account.display_name;
    } else {
      reblogged_display_name = data.account.username
    }
    data = data.reblog;
    data.referenced_by_id = reblogged_by_id;
    data.referenced_display_name = reblogged_display_name;
  }

  if(data.in_reply_to_account_id != null) {
    data.reply_user_name = null
    Mastodon.request("GET", false, "accounts/" + data.in_reply_to_account_id, null, settings.token, function(result) {
      let parsed = JSON.parse(result);

      if(parsed.display_name != "") {
        data.reply_user_name = parsed.display_name;
      } else {
        data.reply_user_name = parsed.username;
      }
    });
  }

  let obj = component.createObject(timelineColumn, {
    dataObject: data
  });
  if (obj == null) {
      // Error Handling
      console.log("Error creating object");
  }
}

function createUserPosts(component, data, filter) {
  let create = true;

  if(filter == "postsonly") {
    if(data.in_reply_to_account_id != null) {
      create = false;
    }
  }

  if(filter == "mediapostsonly") {
    if(data.media_attachments.length == 0) {
      create = false;
    }
  }

  if(create) {
    // If post is actually a reblog, make the reblog the actual post.
    if(data.reblog != null) {
      let reblogged_by_id = data.id
      let reblogged_display_name = ""
      if(data.account.display_name != "") {
        reblogged_display_name = data.account.display_name;
      } else {
        reblogged_display_name = data.account.username
      }
      data = data.reblog;
      data.referenced_by_id = reblogged_by_id;
      data.referenced_display_name = reblogged_display_name;
    }

    if(data.in_reply_to_account_id != null) {
      data.reply_user_name = null
      Mastodon.request("GET", false, "accounts/" + data.in_reply_to_account_id, null, settings.token, function(result) {
        let parsed = JSON.parse(result);

        if(parsed.display_name != "") {
          data.reply_user_name = parsed.display_name;
        } else {
          data.reply_user_name = parsed.username;
        }
      });
    }

    let obj = component.createObject(userPostsColumn, {
      dataObject: data
    });
    if (obj == null) {
        // Error Handling
        console.log("Error creating object");
    }
  }
}

function createDetailedPost(component, data) {
  let obj = component.createObject(postDetailedColumn, {
    dataObject: data,
    detailed: true
  });
  if (obj == null) {
      // Error Handling
      console.log("Error creating object");
  }
}

function createAncestorPosts(ancestors) {
  for(let i=0; i<ancestors.length; i++) {
    let object = ancestors[i];

    createContextPost(Qt.createComponent("../Components/PostObject.qml"), object, "ancestor");
  }
}

function createDescendantPosts(parentID, descendants) {
  for(let i=0; i<descendants.length; i++) {
    let object = descendants[i];

    if(object.in_reply_to_id == parentID) {
      createContextPost(Qt.createComponent("../Components/PostObject.qml"), object, "descendant");
    }
  }
}

function createContextPost(component, data, type) {
  let contextColumn = null;
  if(type == "ancestor") {
    contextColumn = postAncestorColumn;
  } else if(type == "descendant") {
    contextColumn = postDescendantColumn;
  }
  let obj = component.createObject(contextColumn, {
    dataObject: data
  });
  if (obj == null) {
      // Error Handling
      console.log("Error creating object");
  }
}

function createPostImage(data) {
  let dataObject = {}
  dataObject.src = data.preview_url

  let component = Qt.createComponent("./AttachedImage.qml")
  let obj = component.createObject(mediaAttachmentColumn, {
    src: dataObject.src
  });
  if (obj == null) {
      console.log("Error creating object");
  }
}

function createPostLink(component) {

}

function createComment(component, data) {
  let obj = component.createObject(postDetailedColumn, {
    dataObject: data
  });
  console.log("created");
  if (obj == null) {
      // Error Handling
      console.log("Error creating object");
  }
}
