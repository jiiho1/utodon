var component;
var objectParent;

function deserializeData(data) {
  const obj = JSON.parse(data);

  return obj
}

function getFileContent(fileUrl, callback){
   var xhr = new XMLHttpRequest;
   var result = "";
   xhr.open("GET", fileUrl); // set Method and File
   xhr.send(); // begin the request

   xhr.onreadystatechange = function () {
       if(xhr.readyState === XMLHttpRequest.DONE){ // if request_status == DONE
           var response = xhr.responseText;

           if(callback) callback(response);
       }
   }
}

function createView(component, endpoint) {
  let obj = component.createObject(pageMain, {x: 100, y: 100, endpoint: endpoint});
  if (obj == null) {
      // Error Handling
      console.log("Error creating object");
  }

  return obj;
}

function destroyView(component) {
  try {
    component.destroy();
  } catch (e) {
    console.log(e);
  }
}

function showError(error) {
  let component = Qt.createComponent("../Components/Error.qml")
  let obj = component.createObject(errorColumn, {x: 100, y: 100, errorText: error});
  if (obj == null) {
      // Error Handling
      console.log("Error creating object");
  }

  return obj;
}



function linkStyleFormat(content) {
  return content.replace(/<span class="invisible".*?>.*?<\/span>/ig,'').replace(/<a/g, "<a style='color: #19B6EE; text-decoration: none;'");
}

function parseEmojis(emojis, content) {
  let size = units.gu(2)
  let text = content;

  if(emojis != null) {
    for(let i = 0; i < emojis.length; i++) {
      text = text.split(":"+emojis[i].shortcode+":").join('<img height="' + size + '" width="' + size + '" src="'+emojis[i].static_url+'"/>');
    }
  }

  return text;
}

function parseTags(tags, content) {
  let text = content;
  let regEscape = v => v.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&');

  if(tags != null) {
    for(let i = 0; i < tags.length; i++) {
      text = text.split(new RegExp(regEscape(">#<span>" + tags[i].name + "</span>"), "ig")).join('style="display:none;"></a><a href="utodon://tag/?name=' + tags[i].name + '">#<span>' + tags[i].name + '</span>');
    }
  }

  return text;
}

function formatCreationDate(fullDateString) {
  let date = new Date(fullDateString)
  let today = new Date();

  let timeString = ""

  let difference = today.getTime() - date.getTime();
  let seconds = difference / 1000;
  let minutes = seconds / 60;
  let hours = minutes / 60;
  let days = hours / 24;
  if(seconds < 60) {
    timeString = parseInt(seconds, 10) + "s";
  } else if (minutes < 60) {
    timeString = parseInt(minutes, 10) + "m";
  } else if (hours < 24) {
    timeString = parseInt(hours, 10) + "h";
  } else if (days < 30) {
    timeString = parseInt(days, 10) + "d";
  }

  return timeString;
}

function limitString(title, limit) {
  var trimmedString = title.length > limit ?
                    title.substring(0, limit - 3) + "..." :
                    title;
  return trimmedString;
}

function getRelationship(personalId ,id) {
  Mastodon.request("GET", true, "accounts/" + personalId + "/following", null, settings.token, function(result) {
    for(let i = 0; i < data.length; i++) {
      if(data[i].id == id) {
        return data[i];
      }
    }
  });

  return null
}

function roundToReadable(number) {
  if(number / 1000000 > 1) {
    return (Math.round((number / 1000000) * 10) / 10) + "M"
  } else if (number / 1000 > 1) {
    return (Math.round((number / 1000) * 10) / 10) + "K"
  } else {
    return number
  }
}
