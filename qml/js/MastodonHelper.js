function request(type, async, url, data, authentication_token, callback) {
  let xhr = new XMLHttpRequest();

  if(url == "oauth/token") {
    xhr.open(type, "https://" + settings.instance + "/" + url, async);
  } else if (url == "instance") {
    xhr.open(type, "https://" + data + "/api/v1/" + url, async);
  } else {
    xhr.open(type, root.base + url, async);
  }

  // Only specify the content type if POST is used
  if(type == "POST") {
    xhr.setRequestHeader("Accept", "application/json");
    xhr.setRequestHeader("Content-Type", "application/json");
  }

  // Add the authentication token if it already exists
  if(authentication_token != null)
    xhr.setRequestHeader("Authorization", "Bearer " + authentication_token);

  xhr.onreadystatechange = function () {
      if(xhr.readyState === XMLHttpRequest.DONE){
          var response = xhr.responseText;

          if(response != "" && response != null)
            if(callback) callback(response);
          else
            Helper.showError("The server didn't want to tell me.");
      }
  }

  xhr.send(data);
}

function registerApplication() {
  let website = "";
  let scopes = root.scopes

  if(typeof scopes !== "string") {
    scopes = root.scopes.join(" ");
  }

  let url = "apps";
  let appData = {}
  appData.client_name = root.clientName;
  appData.redirect_uris = root.redirectURI;
  appData.scopes = scopes;

  request("POST", true, url, JSON.stringify(appData), null, function(result) {
    console.log(result);
    let data = JSON.parse(result);
    settings.clientID = data.client_id;
    settings.clientSecret = data.client_secret;

    Qt.openUrlExternally(getAuthURL(data.client_id, root.redirectURI, root.scopes.replace(/ /g, "+")));
  });
}

function getAuthURL(client_id, redirect_id, scopes) {
  //console.log("https://" + settings.instance + "/oauth/authorize?client_id=" + client_id + "&redirect_uri=" + redirect_id + "&response_type=code&scope=" + scopes.replace(/ /g, "+"));
  return "https://" + settings.instance + "/oauth/authorize?client_id=" + client_id + "&redirect_uri=" + redirect_id + "&response_type=code&scope=" + scopes.replace(/ /g, "+")
}

function getAccessTokenFromAuthCode(client_id, client_secret, redirect_uri, code, grant_type, scope) {
  let url= "oauth/token";

  let data = {}
  data.client_id = client_id;
  data.client_secret = client_secret;
  data.redirect_uri = redirect_uri;
  data.code = code;
  data.grant_type = grant_type;
  data.scope = scope;

  request("POST", true, url, JSON.stringify(data), null, function(result) {
    let code_response = JSON.parse(result);

    if(code_response.access_token != null) {
      settings.token = code_response.access_token;

      // Load main view after login.
      pageStack.pop();
      pageStack.push(Qt.resolvedUrl("../Pages/MainPage.qml"));
    }
  });
}

function getData(endpoint, type, filter) {
  request("GET", true, endpoint, null, settings.token, function(result) {
    var obj = JSON.parse(result);

    if(Array.isArray(obj) && type != "post_context") {
      for(let i=0; i<obj.length; i++) {
        let object = obj[i];

        if(type == "timeline")
          PostHelper.createPost(Qt.createComponent("../Components/PostObject.qml"), object);
        else if(type == "comment")
          PostHelper.createComment(Qt.createComponent("../Components/CommentObject.qml"), object);
        else if(type == "userPosts")
          PostHelper.createUserPosts(Qt.createComponent("../Components/PostObject.qml"), object, filter);
      }
    } else {
      if(type == "post") {
        PostHelper.createDetailedPost(Qt.createComponent("../Components/PostObject.qml"), obj);
      }
    }
  });
}
