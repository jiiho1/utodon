/*
 * Copyright (C) 2022  Steven Granger
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * utodon is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Lomiri.Components 1.3
//import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import Lomiri.Connectivity 1.0
import "./js/MastodonHelper.js" as Mastodon
import "./js/Helper.js" as Helper

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'utodon.comiryu'
    automaticOrientation: true

    width: units.gu(45)
    height: units.gu(75)

    anchorToKeyboard: true

    property var statusMap: ["Offline", "Connecting", "Online"]

    // These are properties that should not even be changed in the config file.
    readonly property string clientName: "Utodon";
    readonly property var redirectURI: "utodon://registration/";
    readonly property string scopes: "read write follow";

    readonly property string base: "https://" + settings.instance + "/api/v1/";

    // Non-changeable filter words
    // Post with these in the content can be displayed, however they need to
    // actively be allowed
    readonly property var denyFilter: ["incest", "nsfw", "porn"]

    Settings {
        id: settings

        // App registration settings
        property string instance: ""
        property string clientID: ""
        property string clientSecret: ""
        property string token: ""

        // Theme settings
        readonly property string cardColor: theme.name == "Lomiri.Components.Themes.SuruDark" ? "#444444" : "#EAE9E7"
        readonly property int margin: units.gu(2)
        readonly property string subColor: "#888888"

        // App settings
        // Do not display sensitive content by default
        property bool displaySensitiveContent: true

        // Set some default values to be able to allow sensitive content
        // by default. Otherwise even posts with spoilers won't be shown.
        property var denyFilter: ["test"]
        property var allowFilter: []
    }

    PageStack {
      id: pageStack
      Component.onCompleted: {
        //if(Connectivity.online) {
          verifyLogin();
        //}
      }
    }

    Column {
      id: errorColumn

      anchors {
        left: parent.left
        right: parent.right
        bottom: parent.bottom
        topMargin: settings.margin
        bottomMargin: settings.margin
      }

      spacing: units.gu(2)
    }

    Rectangle {
      anchors.fill: parent

      visible: !Connectivity.online && pageStack.currentPage == null

      color: theme.name == "Lomiri.Components.Themes.SuruDark" ? UbuntuColors.jet : UbuntuColors.white

      Rectangle {
        anchors.centerIn: parent

        implicitHeight: offlineIcon.implicitHeight + offlineLabel.implicitHeight

        Icon {
          id: offlineIcon
          anchors.horizontalCenter: parent.horizontalCenter

          width: units.gu(10)
          height: units.gu(10)

          name: "network-wired-offline"
        }

        Label {
          id: offlineLabel
          anchors.horizontalCenter: parent.horizontalCenter
          anchors.top: offlineIcon.bottom
          anchors.topMargin: settings.margin

          text: i18n.tr("You are currently offline.")
        }
      }
    }

    Connections {
        target: UriHandler

        onOpened: {
            console.log('Open from UriHandler');
            console.log(uris[0]);

            if (uris.length > 0) {
              if (uris[0].match(/^utodon:\/\/registration\/\?code=.*/)) {
                  let code = uris[0].replace("utodon://registration/?code=", "");
                  Mastodon.getAccessTokenFromAuthCode(settings.clientID, settings.clientSecret, root.redirectURI, code, "authorization_code", root.scopes);
              } else if(uris[0].match(/^utodon:\/\/tag\/\?name=.*/)) {
                let tag = uris[0].replace("utodon://tag/?name=", "");
                console.log(tag);
              }
            }
        }
    }

    Connections {
        target: Connectivity

        //onStatusChanged: console.log("Status: " + statusMap[Connectivity.status])
        onOnlineChanged: {
          if(Connectivity.online && pageStack.currentPage == null) {
            verifyLogin()
          }
        }
    }

    function verifyLogin() {
      if(settings.token != "") {
        Mastodon.request("GET", true, "accounts/verify_credentials", null, settings.token, function(result) {
          let data = JSON.parse(result);
          if(data.id != null) {
            pageStack.push(Qt.resolvedUrl("./Pages/MainPage.qml"), {userObject: data});
          } else {
            Helper.showError(data.error);
            pageStack.push(Qt.resolvedUrl("./Pages/Login.qml"));
          }
        });
      } else {
        pageStack.push(Qt.resolvedUrl("./Pages/Login.qml"));
      }
    }
}
