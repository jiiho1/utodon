/*
 * Copyright (C) 2022  Comiryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * putaside is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Lomiri.Components 1.3
//import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import QtQuick.LocalStorage 2.0
import Lomiri.Connectivity 1.0
import "../Components"
import "../js/Database.js" as MyDB
import "../js/Helper.js" as Helper
import "../js/MastodonHelper.js" as Mastodon
import "../js/PostHelper.js" as PostHelper

Page {
  id: pagePostDetailed

  property var postObject: null
  property var dataObject: null

  header: PageHeader {
    id: header
    title: i18n.tr("Post from") + " " + Helper.parseEmojis(dataObject.account.emojis, dataObject.account.display_name)

    flickable: postDetailedFlickable
  }

  ProgressBar {
    id: indeterminateBar
    anchors {
      left: parent.left
      right: parent.right
      top: header.bottom
    }

    // Temporarely disable conncetivty check until 20.04 is capable of handling it
    visible: !Connectivity.online

    indeterminate: true
  }

  ScrollView {
      width: parent.width
      height: parent.height
      contentItem: postDetailedFlickable
  }

  Flickable {
      id: postDetailedFlickable

      width: parent.width
      height: parent.height

      contentHeight: postDescendantColumn.height + postAncestorColumn.height + postDetailedColumn.height + units.gu(4)
      topMargin: units.gu(2)

      Column {
          id: postAncestorColumn
          width: parent.width
          anchors.top: parent.top
          anchors.topMargin: units.gu(1)
      }

      Column {
          id: postDetailedColumn
          width: parent.width
          anchors.top: postAncestorColumn.bottom
      }

      Column {
          id: postDescendantColumn
          width: parent.width
          anchors.top: postDetailedColumn.bottom
      }

      onContentHeightChanged: {
        // Scroll to post item after loading context posts
        var postItem = postDetailedColumn.children[0];
        contentY = postItem.mapToItem(postDetailedFlickable.contentItem, 0, 0).y
      }
  }

  Component.onCompleted: {
    // Fetch post context and separate post objects between ancestor and
    // descendant into separate columns to create a timeline experience
    Mastodon.request("GET", true, "statuses/" + dataObject.id, null, settings.token, function(result) {
      var obj = JSON.parse(result);

      PostHelper.createDetailedPost(Qt.createComponent("../Components/PostObject.qml"), obj);
    });

    // Fetch information of post
    Mastodon.request("GET", true, "statuses/" + dataObject.id + "/context", null, settings.token, function(result) {
      var obj = JSON.parse(result);

      PostHelper.createAncestorPosts(obj.ancestors);
      PostHelper.createDescendantPosts(dataObject.id, obj.descendants);
    });
  }
}
