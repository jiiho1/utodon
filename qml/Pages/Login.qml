/*
 * Copyright (C) 2022  Comiryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * putaside is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Lomiri.Components 1.3
//import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import QtQuick.LocalStorage 2.0
import QtGraphicalEffects 1.12
import "../Components"
import "../js/Database.js" as MyDB
import "../js/Helper.js" as Helper
import "../js/MastodonHelper.js" as Mastodon

Page {
  id: pageMain

  property var currentView: null
  property var instanceData: null

  header: PageHeader {
    id: mainHeader
    title: 'Utodon'

    trailingActionBar {
       actions: [
         Action {
            iconName: "info"
            text: i18n.tr("About")

            onTriggered: pageStack.push(Qt.resolvedUrl("./About.qml"))
         }
      ]
    }
  }

  Rectangle {
    anchors.fill: parent

    width: parent.width

    color: "transparent"

    Column {
      anchors.centerIn: parent

      width: parent.width

      spacing: units.gu(2)

      TextField {
        id: instanceTextField

        anchors.horizontalCenter: parent.horizontalCenter

        visible: !loginButton.visible

        placeholderText: i18n.tr("Instance")
      }

      Button {
        id: continueButton

        anchors.horizontalCenter: parent.horizontalCenter

        enabled: instanceTextField.text != ""
        visible: !loginButton.visible

        text: i18n.tr("Continue")

        onClicked: {
          Mastodon.request("GET", true, "instance", instanceTextField.text, settings.token, function(result) {
            console.log(result);
            if(result != "") {
              let data = JSON.parse(result);
              if(data.uri == instanceTextField.text) {
                instanceData = data
                instanceInfoRectangle.visible = true;
                loginButton.visible = true;
                registerButton.visible = true;
              } else {
                Helper.showError(data.error);
              }
            } else {
              Helper.showError("Instance not found");
            }
          });
        }
      }

      Rectangle {
        id: instanceInfoRectangle

        visible: false

        anchors.horizontalCenter: parent.horizontalCenter

        width: parent.width - settings.margin * 2
        height: instanceUriTitle.implicitHeight + instanceDescription.implicitHeight < units.gu(6) ? units.gu(6) : instanceUriTitle.implicitHeight + instanceDescription.implicitHeight

        radius: units.gu(0.5)

        color: "transparent"

        Image {
          id: imageInstance

          anchors {
            left: parent.left
            top: parent.top
          }

          width: units.gu(6)
          height: units.gu(6)

          fillMode: "PreserveAspectCrop"

          source: instanceData.thumbnail

          property bool rounded: true
          property bool adapt: true

          layer.enabled: rounded
          layer.effect: OpacityMask {
              maskSource: Item {
                  width: imageInstance.width
                  height: imageInstance.height
                  Rectangle {
                      anchors.centerIn: parent
                      width: imageInstance.adapt ? imageInstance.width : Math.min(imageInstance.width, imageInstance.height)
                      height: imageInstance.adapt ? imageInstance.height : width
                      radius: units.gu(0.5)
                  }
              }
          }
        }

        Label {
          id: instanceUriTitle
          anchors {
            left: imageInstance.right
            right: parent.right
            top: parent.top
            leftMargin: units.gu(1)
            rightMargin: settings.margin
          }

          text: instanceData.uri
          textSize: Label.Large
          wrapMode: Text.WordWrap
        }

        Label {
          id: instanceDescription
          anchors {
            left: imageInstance.right
            right: parent.right
            top: instanceUriTitle.bottom
            leftMargin: units.gu(1)
            rightMargin: settings.margin
          }

          text: instanceData.description
          wrapMode: Text.WordWrap
          color: settings.subColor
        }
      }

      Button {
        id: loginButton

        anchors.horizontalCenter: parent.horizontalCenter

        visible: false

        text: i18n.tr("Login")

        onClicked: {
          settings.instance = instanceData.uri
          Mastodon.registerApplication();
        }
      }

      Button {
        id: registerButton

        anchors.horizontalCenter: parent.horizontalCenter

        visible: false

        text: i18n.tr("Register")

        onClicked: {
          Qt.openUrlExternally("https://" + instanceData.uri + "/");
        }
      }
    }
  }
}
