/*
 * Copyright (C) 2022  Comiryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * putaside is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Lomiri.Components 1.3
//import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import QtQuick.LocalStorage 2.0
import QtGraphicalEffects 1.12
import Lomiri.Connectivity 1.0
import "../Components"
import "../js/Helper.js" as Helper
import "../js/MastodonHelper.js" as Mastodon
import "../js/PostHelper.js" as PostHelper

Page {
  id: pageSavings

  property string id: ""
  property bool userprofile: false
  property var dataObject: null
  property var relationshipObject: null

  header: PageHeader {
    id: header
    title: dataObject != null ? dataObject.display_name != "" ? Helper.parseEmojis(dataObject.emojis, dataObject.display_name) : dataObject.username : ""

    flickable: userFlickable
  }

  ProgressBar {
    id: indeterminateBar
    anchors {
      left: parent.left
      right: parent.right
      top: header.bottom
    }

    visible: !Connectivity.online

    indeterminate: true
  }

  ScrollView {
    width: parent.width
    height: parent.height
    contentItem: userFlickable
  }

  Flickable {
    id: userFlickable
    width: parent.width
    height: parent.height
    contentHeight: userColumn.height

    Column {
      id: userColumn
      width: parent.width
      anchors.top: parent.top
      spacing: units.gu(2)

      Rectangle {
        id: headerRectangle

        color: settings.subColor

        height: units.gu(25)
        width: parent.width

        Image {
          id: userHeaderImage

          anchors.horizontalCenter: parent.horizontalCenter

          fillMode: "PreserveAspectCrop"
          width: parent.width
          height: parent.height

          source: dataObject != null ? dataObject.header_static : ""
        }

        Image {
          id: userProfilePicture

          anchors {
            left: parent.left
            bottom: parent.bottom
            leftMargin: settings.margin
            bottomMargin: -units.gu(6)
          }

          width: units.gu(12)
          height: units.gu(12)

          source: dataObject != null ? dataObject.avatar_static : ""

          property bool rounded: true
          property bool adapt: true

          layer.enabled: rounded
          layer.effect: OpacityMask {
              maskSource: Item {
                  width: userProfilePicture.width
                  height: userProfilePicture.height
                  Rectangle {
                      anchors.centerIn: parent
                      width: userProfilePicture.adapt ? userProfilePicture.width : Math.min(userProfilePicture.width, userProfilePicture.height)
                      height: userProfilePicture.adapt ? userProfilePicture.height : width
                      radius: units.gu(1)
                  }
              }
          }
        }
      }

      Rectangle {
        anchors {
          left: parent.left
          right: parent.right
        }

        height: followButton.height

        color: "transparent"

        Button {
          id: userMenuButton

          anchors {
            top: parent.top
            right: parent.right
            rightMargin: settings.margin
          }

          width: units.gu(4)
          height: units.gu(4)

          color: "transparent"

          Row {
            anchors.centerIn: parent

            spacing: units.gu(1)

            Icon {
              width: units.gu(3)
              height: units.gu(3)
              name: "contextual-menu"
            }
          }
        }

        Button {
          id: userAlertButton

          visible: !userprofile

          anchors {
            top: parent.top
            right: userMenuButton.left
            rightMargin: settings.margin
          }

          width: units.gu(4)
          height: units.gu(4)

          color: "transparent"

          Row {
            anchors.centerIn: parent

            spacing: units.gu(1)

            Icon {
              width: units.gu(3)
              height: units.gu(3)
              name: "notification"
            }
          }
        }

        Button {
          id: followButton

          visible: !userprofile

          anchors {
            top: parent.top
            right: userAlertButton.left
            rightMargin: units.gu(3)
          }

          height: units.gu(4)

          text: i18n.tr("Follow")
          color: theme.palette.normal.positive
        }
      }

      Rectangle {
        id: userInfoRectangle
        anchors{
          left: parent.left
          right: parent.right
          leftMargin: settings.margin
          rightMargin: settings.margin
        }

        color: "transparent"

        height: displayNameLabel.implicitHeight + accountLabel.implicitHeight + noteLabel.implicitHeight + fieldsListViewRectangle.height + joinedLabel.implicitHeight + countRow.height + (settings.margin * 2)

        Label {
          id: displayNameLabel
          anchors {
            left: parent.left
            top: parent.top
          }

          text: dataObject != null ? dataObject.display_name != "" ? Helper.parseEmojis(dataObject.emojis, dataObject.display_name) : dataObject.username : ""
          textSize: Label.Large
        }

        Label {
          id: accountLabel

          anchors {
            left: parent.left
            top: displayNameLabel.bottom
          }

          text: dataObject != null ? "@" + dataObject.acct : ""
          textSize: Label.Small
          color: settings.subColor
        }

        Label {
          id: noteLabel
          anchors {
            top: accountLabel.bottom
            left: parent.left
            right: parent.right
            topMargin: settings.margin
          }

          text: dataObject != null ? Helper.linkStyleFormat(Helper.parseTags(dataObject.tags, Helper.parseEmojis(dataObject.emojis, dataObject.note))) : ""
          onLinkActivated: Qt.openUrlExternally(link)
          wrapMode: Text.Wrap
          textFormat: Text.RichText
        }

        Rectangle {
          id: fieldsListViewRectangle
          anchors {
            left: parent.left
            right: parent.right
            top: noteLabel.bottom
            topMargin: settings.margin
          }

          radius: units.gu(1)
          color: settings.cardColor

          height: fieldsListView.contentHeight

          ListView {
            id: fieldsListView
            anchors.fill: parent
            anchors.leftMargin: units.gu(1)
            anchors.rightMargin: units.gu(1)
            anchors.topMargin: units.gu(1.5)
            anchors.bottomMargin: units.gu(1.5)

            // make the list non scrollable
            interactive: false

            model: fieldsListModel
            delegate: fieldsDelegate
          }
        }

        Label {
          id: joinedLabel
          anchors {
            top: fieldsListViewRectangle.bottom
            left: parent.left
            right: parent.right
            topMargin: settings.margin
          }

          text: dataObject != null ? "Joined " + new Date(dataObject.created_at).toLocaleDateString() : ""
          textSize: Label.Small
          wrapMode: Text.Wrap
          color: settings.subColor
        }

        Row {
          id: countRow

          anchors {
            top: joinedLabel.bottom
            left: parent.left
            right: parent.right
            topMargin: settings.margin
          }

          spacing: settings.margin
          height: Math.max(statusesCountRectangle.height) + units.gu(4);

          Rectangle {
            id: statusesCountRectangle

            width: Math.max(statusesCount.implicitWidth, statusesCountLabel.implicitWidth)
            height: statusesCount.implicitHeight + statusesCountLabel.implicitHeight

            color: "transparent"

            Label{
              id: statusesCount

              anchors {
                top: parent.top
                left: parent.left
              }

              text: Helper.roundToReadable(dataObject.statuses_count)
              textSize: Label.Large
            }

            Label{
              id: statusesCountLabel

              anchors {
                top: statusesCount.bottom
                left: parent.left
              }

              text: i18n.tr("posts")
              textSize: Label.Small
            }
          }

          Rectangle {
            id: followingCountRectangle


            width: Math.max(followingCount.implicitWidth, followingCountLabel.implicitWidth)
            height: followingCount.implicitHeight + followingCountLabel.implicitHeight

            color: "transparent"

            Label{
              id: followingCount

              anchors {
                top: parent.top
                left: parent.left
              }

              text: Helper.roundToReadable(dataObject.following_count)
              textSize: Label.Large
            }

            Label{
              id: followingCountLabel

              anchors {
                top: followingCount.bottom
                left: parent.left
              }

              text: i18n.tr("following")
              textSize: Label.Small
            }
          }

          Rectangle {
            id: followersCountRectangle

            width: Math.max(followersCount.implicitWidth, followersCountLabel.implicitWidth)
            height: followersCount.implicitHeight + followersCountLabel.implicitHeight

            color: "transparent"

            Label{
              id: followersCount

              anchors {
                top: parent.top
                left: parent.left
              }

              text: Helper.roundToReadable(dataObject.followers_count)
              textSize: Label.Large
            }

            Label{
              id: followersCountLabel

              anchors {
                top: followersCount.bottom
                left: parent.left
              }

              text: i18n.tr("followers")
              textSize: Label.Small
            }
          }
        }
      }

      Sections {
        id: userPostsSection

        anchors {
          left: parent.left
          right: parent.right
        }

        model: [ i18n.tr("Posts"), i18n.tr("Posts & Replies"), i18n.tr("Media") ]
        onSelectedIndexChanged: {
          if(Connectivity.online && dataObject.statuses_count > 0) {
            for(let i = 0; i < userPostsColumn.children.length; i++) {
              userPostsColumn.children[i].destroy();
            }
            if(selectedIndex == 0) {
              Mastodon.getData("accounts/" + id + "/statuses?limit=5", "userPosts", "postsonly");
            } else if(selectedIndex == 1) {
              Mastodon.getData("accounts/" + id + "/statuses?limit=5", "userPosts", null);
            } else if(selectedIndex == 2) {
              Mastodon.getData("accounts/" + id + "/statuses?limit=5&only_media=true", "userPosts", null);
            }
          }
        }

        Component.onCompleted: Mastodon.getData("accounts/" + id + "/statuses?limit=5", "userPosts", "postsonly");
      }

      Column {
        id: userPostsColumn

        width: parent.width
      }
    }

    onAtYEndChanged: {
      if(atYEnd) {
        let lastObjectID = userPostsColumn.children[userPostsColumn.children.length -1].dataObject.id
        if(userPostsSection.selectedIndex == 0) {
          Mastodon.getData("accounts/" + id + "/statuses?limit=5&max_id=" + lastObjectID, "userPosts", "postsonly");
        } else if(userPostsSection.selectedIndex == 1) {
          Mastodon.getData("accounts/" + id + "/statuses?limit=5&max_id=" + lastObjectID, "userPosts", null);
        } else if(userPostsSection.selectedIndex == 2) {
          Mastodon.getData("accounts/" + id + "/statuses?limit=5&only_media=true&max_id=" + lastObjectID, "userPosts", null);
        }
      }
    }
  }

  Button {
    anchors {
      right: parent.right
      bottom: parent.bottom
      rightMargin: settings.margin
      bottomMargin: settings.margin
    }

    visible: !userFlickable.atYBeginning

    height: units.gu(7)
    width: units.gu(7)

    opacity: 0.7

    Icon {
      anchors.centerIn: parent
      height: units.gu(4)
      width: units.gu(4)
      name: "up"
      color: UbuntuColors.silk
    }

    onClicked: {
      var firstItem = headerRectangle;
      userFlickable.contentY = -42
    }
  }

  Component.onCompleted: {
    Mastodon.request("GET", true, "accounts/" + id, null, settings.token, function(result) {
      dataObject = JSON.parse(result);

      // Generate user fields after dataObject got created
      for(let i = 0; i < dataObject.fields.length; i++) {
        fieldsListModel.append({fieldName: dataObject.fields[i].name, fieldContent: dataObject.fields[i].value});
      }
    });

    // Only fetch the relationship if the account is not the own account
    if(userprofile == false) {
      // Update following status in UI
      Mastodon.request("GET", true, "accounts/relationships?id=" + id, null, settings.token, function(result) {
        // Only get first array object since we are only interested in the current account
        let relationship = JSON.parse(result)[0];

        if(relationship.following == true) {
          followButton.color = theme.palette.normal.negative
          followButton.text = i18n.tr("Unfollow")
        }
      })
    }
  }

  /*
  User fields Components
  */
  Component{
    id: fieldsDelegate

    Rectangle {
      anchors {
        left: parent.left
        right: parent.right
        leftMargin: units.gu(1)
        rightMargin: units.gu(1)
      }

      height: fieldNameLabel.implicitHeight + fieldContentLabel.implicitHeight + units.gu(3.5)

      color: "transparent"

      Label {
        id: fieldNameLabel

        anchors {
          top: parent.top
          left: parent.left
          right: parent.right
        }

        text: fieldName
        textSize: Label.Small
        color: settings.subColor
      }

      Label {
        id: fieldContentLabel

        anchors {
          top: fieldNameLabel.bottom
          left: parent.left
          right: parent.right
          bottom: parent.bottom
          topMargin: units.gu(0.5)
        }

        text: dataObject != null ? Helper.linkStyleFormat(Helper.parseTags(dataObject.tags, Helper.parseEmojis(dataObject.emojis, fieldContent))) : ""
        onLinkActivated: Qt.openUrlExternally(link)
        wrapMode: Text.Wrap
        textFormat: Text.RichText
      }
    }
  }

  ListModel {
    id: fieldsListModel
  }
}
