/*
 * Copyright (C) 2022  Comiryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * putaside is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Lomiri.Components 1.3
//import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import QtQuick.LocalStorage 2.0
import Lomiri.Connectivity 1.0
import "../Components"
import "../js/Database.js" as MyDB
import "../js/Helper.js" as Helper
import "../js/MastodonHelper.js" as Mastodon
import "../js/PostHelper.js" as PostHelper

Item {
  property string endpoint: ""
  property string searchTerm: ""

  anchors {
    top: mainHeader.bottom
    left: parent.left
    right: parent.right
    bottom: pageRectangle.top
  }

  ScrollView {
      width: parent.width
      height: parent.height
      contentItem: timelineFlickable
  }

  Flickable {
        id: timelineFlickable

        width: parent.width
        height: parent.height

        contentHeight: timelineColumn.height + units.gu(4)
        topMargin: units.gu(2)

        Column {
            id: timelineColumn
            width: parent.width
            anchors.top: parent.top
            anchors.topMargin: units.gu(1)

            Rectangle {
              anchors {
                left: parent.left
                right: parent.right
                bottomMargin: -settings.margin
                topMargin: -settings.margin
              }

              height: searchTextField.height + settings.margin*2

              visible: endpoint == "timelines/public" || endpoint.includes("timelines/tag/")

              color: "transparent"

              TextField {
                id: searchTextField

                anchors {
                  left: parent.left
                  top: parent.top
                  bottom: parent.bottom
                  right: searchButton.left
                  leftMargin: settings.margin
                  topMargin: settings.margin
                  bottomMargin: settings.margin
                  rightMargin: units.gu(1)
                }

                placeholderText: i18n.tr("Search")
              }

              Button {
                id: searchButton

                anchors {
                  right: parent.right
                  top: parent.top
                  bottom: parent.bottom
                  rightMargin: settings.margin
                  topMargin: settings.margin
                  bottomMargin: settings.margin
                }

                width: units.gu(4)

                Row {
                  anchors.centerIn: parent

                  spacing: units.gu(1)

                  Icon {
                    width: units.gu(2)
                    height: units.gu(2)
                    name: "toolkit_input-search"

                    color: "white"
                  }
                }
              }
            }

            Label {
              anchors.horizontalCenter: parent.horizontalCenter
              anchors.topMargin: settings.margin

              visible: timelineColumn.children.length < 3 && endpoint == "timelines/home"

              text: i18n.tr("There are no posts to display yet.")
              textSize: Label.Large
              wrapMode: Text.WordWrap
              color: settings.subColor
            }
        }

        PullToRefresh {
            enabled: Connectivity.online

            parent: timelineFlickable
            refreshing: timelineColumn.children.length < 4
            onRefresh: reload()
        }

        onAtYEndChanged: {
          if(atYEnd) {
            let lastObjectID = timelineColumn.children[timelineColumn.children.length -1].dataObject.id
            Mastodon.getData(endpoint + "?limit=5&max_id=" + lastObjectID, "timeline", null);
          }
        }

        Component.onCompleted: {
          Mastodon.getData(endpoint, "timeline", null);
        }
    }

    Button {
      anchors {
        right: parent.right
        bottom: parent.bottom
        rightMargin: settings.margin
        bottomMargin: settings.margin
      }

      visible: !timelineFlickable.atYBeginning

      height: units.gu(7)
      width: units.gu(7)

      opacity: 0.7

      Icon {
        anchors.centerIn: parent
        height: units.gu(4)
        width: units.gu(4)
        name: "up"

        color: LomiriColors.silk
      }

      onClicked: {
        var firstItem = timelineColumn.children[0];
        timelineFlickable.contentY = -42
      }
    }

    function reload() {
      for(var i = timelineColumn.children.length; i > 1 ; i--) {
        timelineColumn.children[i-1].destroy()
      }

      Mastodon.getData(endpoint, "timeline", null);
    }
}
