/*
 * Copyright (C) 2022  Comiryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * putaside is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Lomiri.Components 1.3
//import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import QtQuick.LocalStorage 2.0
import "../Components"

Page {
  id: pageSettings

  header: PageHeader {
    title: i18n.tr('Settings')

    flickable: settingsFlickable

    trailingActionBar {
       actions: [
         Action {
           iconName: "info"
           text: i18n.tr("About")

           onTriggered: pageStack.push(Qt.resolvedUrl("./About.qml"))
         }
      ]
    }
  }

  ScrollView {
    width: parent.width
    height: parent.height
    contentItem: settingsFlickable
  }

  Flickable {
    id: settingsFlickable
    width: parent.width
    height: parent.height
    contentHeight: mainColumn.height
    topMargin: units.gu(2)

    Column {
      id: mainColumn
      width: parent.width
      anchors.top: parent.top
      anchors.topMargin: units.gu(1)
      spacing: units.gu(2)

      Label {
        id: postsSettingsLable
        anchors {
          left: parent.left
          leftMargin: settings.margin
        }

        text: i18n.tr("Posts")
        textSize: Label.Large
      }

      Rectangle {
        anchors {
          left: parent.left
          right: parent.right
          leftMargin: settings.margin
          rightMargin: settings.margin
          bottomMargin: units.gu(1)
        }

        color: "transparent"
        implicitHeight: sensitiveContentLabel.implicitHeight

        Label {
          id: sensitiveContentLabel

          anchors {
            left: parent.left
            right: syncSensitiveContentSwitch.left
            top: parent.top
            rightMargin: settings.margin
          }

          wrapMode: Text.Wrap
          text: i18n.tr("Display sensitive content")
        }

        Label {
          id: sensitiveContentInfoLabel

          anchors {
            left: parent.left
            right: syncSensitiveContentSwitch.left
            top: sensitiveContentLabel.bottom
            rightMargin: settings.margin
          }

          wrapMode: Text.Wrap
          text: i18n.tr("Disabling this, will also cause posts with spoilers to not be loaded, as they are marked as sensitive content.")
          textSize: Label.Small
          color: settings.subColor
        }

        Switch {
          id: syncSensitiveContentSwitch

          anchors {
            right: parent.right
            top: parent.top
          }

          checked: settings.displaySensitiveContent

          onClicked: settings.displaySensitiveContent = checked
        }

        Label {
          id: denyFilterLabel

          anchors {
            left: parent.left
            right: parent.right
            top: sensitiveContentInfoLabel.bottom
            topMargin: settings.margin
          }

          wrapMode: Text.Wrap
          text: i18n.tr("Filter: Deny")
        }

        Rectangle {
          id: addDenyFilterRectangle

          anchors {
            left: parent.left
            right: parent.right
            top: denyFilterLabel.bottom
            topMargin: settings.margin
          }

          radius: units.gu(1)
          color: settings.cardColor

          height: units.gu(6)

          Icon {
            anchors.centerIn: parent

            name: "add"
            height: units.gu(4)
            width: units.gu(4)
          }

          TextField {
            id: addDenyFilterTextField

            visible: false

            anchors {
              left: parent.left
              right: addDenyFilterButton.left
              bottom: parent.bottom
              top: parent.top
              leftMargin: settings.margin
              rightMargin: settings.margin
              topMargin: units.gu(1)
              bottomMargin: units.gu(1)
            }

          }

          Button {
            id: addDenyFilterButton

            visible: addDenyFilterTextField.visible

            anchors.right: parent.right
            anchors.rightMargin: settings.margin
            anchors.top: parent.top
            anchors.topMargin: units.gu(1)

            height: units.gu(4)
            width: units.gu(4)

            color: "transparent"

            Icon {
              anchors.fill: parent

              name: "ok"
            }

            onClicked: {
              if(addDenyFilterTextField.text != null && addDenyFilterTextField.text != "") {
                denyFilterListModel.append({word: addDenyFilterTextField.text});
                saveFilterList("deny");
              }

              addDenyFilterTextField.text = "";
              addDenyFilterTextField.visible = false;
            }
          }

          MouseArea {
            anchors.fill: parent

            enabled: !addDenyFilterTextField.visible

            onClicked: {
              addDenyFilterTextField.visible = true;
            }
          }
        }

        Rectangle {
          id: denyFilterListViewRectangle
          anchors {
            left: parent.left
            right: parent.right
            top: addDenyFilterRectangle.bottom
            topMargin: settings.margin
          }

          radius: units.gu(1)
          color: settings.cardColor

          height: filterListView.contentHeight < units.gu(24) ? filterListView.contentHeight : units.gu(24)

          ListView {
            id: filterListView
            anchors.fill: parent

            model: denyFilterListModel
            delegate: filterDelegate
          }
        }
      }
    }
  }

  Component{
    id: filterDelegate

    Rectangle {
      anchors {
        left: parent.left
        right: parent.right
        leftMargin: settings.margin
      }

      height: units.gu(6)

      color: "transparent"

      Label {
        anchors {
          left: parent.left
          right: filterDeleteButton.left
          rightMargin: settings.margin
          verticalCenter: parent.verticalCenter
        }

        text: word
      }

      Button {
        id: filterDeleteButton

        anchors {
          right: parent.right
          rightMargin: settings.margin
          verticalCenter: parent.verticalCenter
        }

        color: "transparent"

        height: units.gu(3)
        width: units.gu(3)

        Icon {
          anchors.fill: parent

          name: "delete"
        }

        onClicked: {
          denyFilterListModel.remove(index);
          saveFilterList("deny");
        }
      }
    }
  }

  ListModel {
    id: denyFilterListModel

    Component.onCompleted: {
      for(let i = 0; i < settings.denyFilter.length; i++) {
        denyFilterListModel.append({word: settings.denyFilter[i]});
      }
    }
  }

  function saveFilterList(type) {
    let newFilterList = []

    switch(type) {
    case "deny":
      for(let i=0; i<denyFilterListModel.count; i++) {
        console.log(denyFilterListModel.get(i).word);
        newFilterList.push(denyFilterListModel.get(i).word)
      }
    }

    settings.denyFilter = newFilterList
  }
}
