/*
 * Copyright (C) 2022  Comiryu
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * putaside is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Lomiri.Components 1.3
//import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import QtQuick.LocalStorage 2.0
import QtGraphicalEffects 1.12
import Lomiri.Connectivity 1.0
import "../Components"
import "../js/Database.js" as MyDB
import "../js/Helper.js" as Helper
import "../js/MastodonHelper.js" as Mastodon

Page {
  id: pageMain

  property var currentView: null
  property var userObject: null

  header: PageHeader {
    id: mainHeader
    title: 'Utodon'

    trailingActionBar {
       actions: [
         Action {
           iconName: "settings"
           text: i18n.tr("Settings")

           onTriggered: pageStack.push(Qt.resolvedUrl("./Settings.qml"))
         }
      ]
    }
  }

  ProgressBar {
    id: indeterminateBar

    anchors {
      left: parent.left
      right: parent.right
      top: mainHeader.bottom
    }

    visible: !Connectivity.online

    indeterminate: true
  }

  Rectangle {
    id: pageRectangle

    anchors {
      bottom: parent.bottom
      left: parent.left
      right: parent.right
    }

    height: units.gu(8)

    color: "transparent"

    Rectangle {
      id: homeButton

      anchors {
        top: parent.top
        bottom: parent.bottom
        left: parent.left
      }

      width: parent.width/4

      color: settings.cardColor

      Row {
        anchors.centerIn: parent

        spacing: units.gu(1)

        Icon {
          width: units.gu(3)
          height: units.gu(3)
          name: "home"
        }
      }

      MouseArea {
          anchors.fill: parent

          enabled: !detailed

          onClicked: {
            homeButton.color = settings.cardColor
            searchButton.color = "transparent"
            notificationButton.color = "transparent"
            accountButton.color = "transparent"

            if(Connectivity.online ) {
              Helper.destroyView(currentView);
              currentView = Helper.createView(Qt.createComponent("../Pages/Timeline.qml"), "timelines/home?limit=5")
            }
          }
      }
    }

    Rectangle {
      id: searchButton

      anchors {
        top: parent.top
        bottom: parent.bottom
        left: homeButton.right
      }

      width: parent.width/4

      color: "transparent"

      Row {
        anchors.centerIn: parent

        spacing: units.gu(1)

        Icon {
          width: units.gu(3)
          height: units.gu(3)
          name: "toolkit_input-search"
        }
      }

      MouseArea {
          anchors.fill: parent

          enabled: !detailed

          onClicked: {
            homeButton.color = "transparent"
            searchButton.color = settings.cardColor
            notificationButton.color = "transparent"
            accountButton.color = "transparent"

            if(Connectivity.online ) {
              Helper.destroyView(currentView);
              currentView = Helper.createView(Qt.createComponent("../Pages/Timeline.qml"), "timelines/public?limit=5")
            }
          }
      }
    }

    Rectangle {
      id: notificationButton

      anchors {
        top: parent.top
        bottom: parent.bottom
        left: searchButton.right
      }

      width: parent.width/4

      color: "transparent"

      Row {
        anchors.centerIn: parent

        spacing: units.gu(1)

        Icon {
          width: units.gu(3)
          height: units.gu(3)
          name: "notification"
        }
      }

      MouseArea {
          anchors.fill: parent

          enabled: !detailed

          onClicked: {
            homeButton.color = "transparent"
            searchButton.color = "transparent"
            notificationButton.color = settings.cardColor
            accountButton.color = "transparent"
          }
      }
    }

    Rectangle {
      id: accountButton

      anchors {
        top: parent.top
        bottom: parent.bottom
        left: notificationButton.right
      }

      width: parent.width/4

      color: "transparent"

      Row {
        anchors.centerIn: parent

        spacing: units.gu(1)

        Icon {
          width: units.gu(3)
          height: units.gu(3)

          visible: userObject == null

          name: "account"
        }

        Image {
          id: userAvatar
          width: units.gu(3)
          height: units.gu(3)

          visible: userObject != null

          source: userObject.avatar_static

          property bool rounded: true
          property bool adapt: true

          layer.enabled: rounded
          layer.effect: OpacityMask {
              maskSource: Item {
                  width: userAvatar.width
                  height: userAvatar.height
                  Rectangle {
                      anchors.centerIn: parent
                      width: userAvatar.adapt ? userAvatar.width : Math.min(userAvatar.width, userAvatar.height)
                      height: userAvatar.adapt ? userAvatar.height : width
                      radius: units.gu(3)
                  }
              }
          }
        }
      }

      MouseArea {
          anchors.fill: parent

          enabled: !detailed

          onClicked: {
            if(Connectivity.online) {
              pageStack.push(Qt.resolvedUrl("../Pages/User.qml"), {id: userObject.id, userprofile: true})
            }
          }
      }
    }
  }

  Component.onCompleted: currentView = Helper.createView(Qt.createComponent("../Pages/Timeline.qml"), "timelines/home?limit=5")
}
